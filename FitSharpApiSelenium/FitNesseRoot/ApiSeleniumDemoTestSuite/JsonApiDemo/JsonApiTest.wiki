---
Suites: api
---
!| Script:  JsonRestApiFixture                                                                  |
|note                  |Test Case 1 - POST REQUEST                                              |
|Url                   |http://fakerestapi.azurewebsites.net/api/Authors                        |
|RequestPayload        |{"ID": 10,"IDBook": "123123213","FirstName": "John","LastName": "Smith"}|
|RequestContentType    |application/json                                                        |
|AddHeaderNameValue;   |Accept                             |application/json                    |
|PostRequest                                                                                    |
|Check                 |ResponseStatus                     |OK                                  |
|Check                 |ResponseSuccess                    |true                                |
|Show                  |ResponseContent                                                         |
|Check                 |ResponseContent                    |=~/123123/                          |
|Check                 |ResponseContent                    |=~/"ID":10/                         |
|Check                 |ResponseContent                    |=~/"ID":13/                         |
|AssertResponseContains|"ID":11                                                                 |

!| Script:  JsonRestApiFixture                                          |
|note                  |Test Case 2 - GET REQUEST                       |
|Url                   |http://fakerestapi.azurewebsites.net/api/Authors|
|AddHeaderNameValue;   |Accept                 |application/json        |
|GetRequest                                                             |
|Check                 |ResponseStatus         |OK                      |
|Check                 |ResponseSuccess        |true                    |
|AssertResponseContains|"ID":11                                         |

!| Script:  JsonRestApiFixture                                                         |
|note               |Test Case 3                                                       |
|Url                |https://www.w3schools.com/xml/tempconvert.asmx/FahrenheitToCelsius|
|RequestPayload;    |Fahrenheit=34                                                     |
|RequestContentType;|application/x-www-form-urlencoded                                 |
|PostRequest                                                                           |
|Show               |ResponseContent                                                   |
|Check              |ResponseContent                     |=~/1.12/                     |


!| Decision:  JsonRestApiFixture                                                                                                                  |
|Url                                                               |RequestPayload|RequestContentType               |PostRequest?|ResponseContent?|
|https://www.w3schools.com/xml/tempconvert.asmx/FahrenheitToCelsius|Fahrenheit=34 |application/x-www-form-urlencoded|            |=~/1.11/        |
|https://www.w3schools.com/xml/tempconvert.asmx/FahrenheitToCelsius|Fahrenheit=33 |application/x-www-form-urlencoded|            |=~/1.11/        |