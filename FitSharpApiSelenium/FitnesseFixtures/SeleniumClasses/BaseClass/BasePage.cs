﻿using OpenQA.Selenium;

namespace SeleniumFixtures.BaseClass
{
    public class BasePage<M>
        where M : BasePageElementMap, new()
    {
        protected readonly string url;

        public BasePage(string url)
        {
            this.url = url;
        }

        public BasePage()
        {
            url = null;
        }

        protected M Map

        {
            get
            {
                return new M();
            }
        }

        public virtual void Navigate(string newUrl)
        {
            if (Browser.Driver != null)
            {
                Browser.Driver.Navigate().GoToUrl(newUrl);
            }
            else
            {
                throw new WebDriverException("Driver not initialized");
            }
        }
    }
}