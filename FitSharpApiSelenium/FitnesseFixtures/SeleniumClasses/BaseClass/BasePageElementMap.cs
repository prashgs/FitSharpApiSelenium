﻿using OpenQA.Selenium;

namespace SeleniumFixtures.BaseClass
{
    public class BasePageElementMap
    {
        public IWebDriver Driver;

        public BasePageElementMap()
        {
            Driver = Browser.Driver;
        }
    }
}