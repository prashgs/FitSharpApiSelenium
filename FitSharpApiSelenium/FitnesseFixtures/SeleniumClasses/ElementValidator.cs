﻿using OpenQA.Selenium;
using SeleniumFixtures.Extension;

namespace SeleniumFixtures.Validators
{
    public class ElementValidator
    {
        public bool ElementPresent(string searchBy, string findString)
        {
            bool present = false;
            switch (searchBy.ToLower())
            {
                case "id":
                    present = (Browser.Driver.FindElementAfterWait(By.Id(findString)) == null) ? false : true;
                    break;

                case "xpath":
                    present = (Browser.Driver.FindElementAfterWait(By.XPath(findString)) == null) ? false : true;
                    break;

                case "name":
                    present = (Browser.Driver.FindElementAfterWait(By.Name(findString)) == null) ? false : true;
                    break;
            }
            return present;
        }
    }
}