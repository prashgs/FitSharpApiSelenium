﻿using OpenQA.Selenium.Chrome;

namespace SeleniumFixtures
{
    public class ChromeBrowserOptions: ChromeOptions
    {
        public ChromeBrowserOptions()
        {
            this.AddArgument("incognito");
            this.AddArgument("--start-maximized");
            //this.AddArgument("--headless");
        }

    }
}