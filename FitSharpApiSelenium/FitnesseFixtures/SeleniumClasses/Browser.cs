﻿using FitnesseFixtures.SeleniumFixture.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace SeleniumFixtures
{
    public static class Browser
    {
        public static IWebDriver Driver { get; set; }

        public static IWebDriver Init(string type = "chrome")
        {
            try
            {
                switch (type.ToLower())
                {
                    case "ie":
                        Driver = new InternetExplorerDriver();
                        break;
                    case "chrome":
                        Uri uri = new System.Uri((Assembly.GetExecutingAssembly().GetName().CodeBase));
                        string chromeDriverLocation = Path.GetDirectoryName(uri.LocalPath);
                        ChromeBrowserOptions chromeBrowserOptions = new ChromeBrowserOptions();
                        Driver = new ChromeDriver(chromeBrowserOptions);
                        break;
                    case "firefox":
                        var firefoxOption = new FirefoxOptions();
                        Driver = new FirefoxDriver();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                Driver = null;
            }
            return Driver;
        }

        public static void Goto(string url)
        {
            Driver.Navigate().GoToUrl(url);
        }

        public static void Close()
        {
            if (Driver != null)
                Driver.Quit();
        }
    }
}