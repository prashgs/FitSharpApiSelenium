﻿using AventStack.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnesseFixtures.SeleniumClasses
{
    public class ReportManager
    {
        private static readonly ExtentReports instance = new ExtentReports();

        private static ExtentTest Test { get; set; }
        private static ExtentTest Node { get; set; }

        static ReportManager()
        {

        }

        public static ExtentReports Instance
        {
            get
            {
                return instance;
            }
        }

        public static void CreateTest(string testName)
        {
            if (!string.IsNullOrEmpty(testName))
            {
                Test = Instance.CreateTest(testName);
            }
        }

        public static void CreateNode(string nodeName, string desctiption=null)
        {
            if (Test!=null)
            {
                if (!string.IsNullOrEmpty(nodeName))
                {
                    Node = Test.CreateNode(nodeName,desctiption);
                }
            }

        }
    }
}
