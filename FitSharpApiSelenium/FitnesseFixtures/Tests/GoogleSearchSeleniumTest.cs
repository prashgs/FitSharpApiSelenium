﻿using FitnesseFixtures.Helpers;
using NUnit.Framework;
using SeleniumFixtures;
using SeleniumFixtures.Pages;
using System.Linq;
using System.Collections.Generic;

namespace FitnesseFixtures.Tests
{
    [TestFixture]
    public class GoogleSearchSeleniumTest
    {
        private TestData testData;

        [SetUp]
        public void SetUp()
        {
            testData = JsonHelper.ReadJson<TestData>(FileHelper.AssemblyPath + @"\TestData\TestData.json");
            Browser.Init(testData.Browser);
        }

        [Test]
        public void GoogleSearchValidResults()
        {
            Browser.Goto(testData.Url);
            GoogleHomePage searchPage = new GoogleHomePage();
            foreach (var searchText in testData.SearchTexts)
            {
                searchPage.Search(searchText);
            }
        }

        [TearDown]
        public void TearDown()
        {
            Browser.Driver.Quit();
        }
    }
}