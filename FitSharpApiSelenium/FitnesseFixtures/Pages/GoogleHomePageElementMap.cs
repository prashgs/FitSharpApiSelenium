﻿using OpenQA.Selenium;
using SeleniumFixtures.BaseClass;
using SeleniumFixtures.Extension;

namespace SeleniumFixtures.ElementMaps
{
    public class GoogleHomePageElementMap : BasePageElementMap
    {
        public IWebElement SearchBox
        {
            get
            {
                return Driver.FindElementAfterWait(By.Name("q"));
            }
        }

        public IWebElement SearchButton
        {
            get
            {
                return Driver.FindElementAfterWait(By.Name("btnK"));
            }
        }
    }
}