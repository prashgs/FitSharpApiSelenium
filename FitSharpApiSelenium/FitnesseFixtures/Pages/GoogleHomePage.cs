﻿using SeleniumFixtures.BaseClass;
using SeleniumFixtures.ElementMaps;
using SeleniumFixtures.Extension;

namespace SeleniumFixtures.Pages
{
    public class GoogleHomePage : BasePage<GoogleHomePageElementMap>
    {
        public void Search(string text)
        {
            Map.SearchBox.ClearAndSendKeys(text);
            Map.SearchButton.Click();
        }
    }
}