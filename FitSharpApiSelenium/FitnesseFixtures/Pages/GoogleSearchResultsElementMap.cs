﻿using OpenQA.Selenium;
using SeleniumFixtures.BaseClass;
using SeleniumFixtures.Extension;

namespace SeleniumFixtures.ElementMaps
{
    public class GoogleSearchResultsElementMap : BasePageElementMap
    {
        public IWebElement Results
        {
            get
            {
                return Driver.FindElementAfterWait(By.Id("res"));
            }
        }
    }
}