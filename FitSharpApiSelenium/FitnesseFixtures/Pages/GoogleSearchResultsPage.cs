﻿using SeleniumFixtures.BaseClass;
using SeleniumFixtures.ElementMaps;

namespace SeleniumFixtures.Pages
{
    public class GoogleSearchResultsPage : BasePage<GoogleSearchResultsElementMap>
    {
        public bool IsResultPresent()
        {
            return Map.Results != null ? true : false;
        }
    }
}