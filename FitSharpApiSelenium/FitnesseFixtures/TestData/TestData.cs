﻿namespace FitnesseFixtures
{
    public class TestData
    {
        public string Browser { get; set; }
        public string Url { get; set; }
        public string[] SearchTexts { get; set; }
    }
}