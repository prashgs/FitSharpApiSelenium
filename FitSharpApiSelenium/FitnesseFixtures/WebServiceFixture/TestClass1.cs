﻿using ApiServiceFixtures;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FitnesseFixtures.WebServiceFixture
{
    [TestClass]
    public class TestClass1
    {
        [TestMethod]
        public void TestMethod()
        {
            JsonRestApiFixture jsonRestApiFixture = new JsonRestApiFixture();
            jsonRestApiFixture.Url = "https://www.w3schools.com/xml/tempconvert.asmx/FahrenheitToCelsius";
            string text = "Fahrenheit=32";
            jsonRestApiFixture.RequestPayload(text);
            jsonRestApiFixture.RequestContentType("application/x-www-form-urlencoded");
            jsonRestApiFixture.PostRequest();
        }
    }
}