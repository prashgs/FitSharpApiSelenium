﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace ApiServiceFixtures
{
    public class JsonRestApiFixture
    {
        private string _requestType;
        private string _requestFormat;
        private string _Requestresponse;

        public JsonRestApiFixture()
        {
            httpClient = new HttpClient();
        }

        private HttpClient httpClient { get; set; }

        public string requestStatus
        {
            get;
            set;
        }

        public string StatusDescriptions
        {
            get;
            set;
        }

        public string Requestresponse
        {
            get { return _Requestresponse; }
            set { _Requestresponse = value; }
        }

        public string RequestType
        {
            get { return _requestType; }
            set { _requestType = value; }
        }

        public string requestFormat
        {
            get { return _requestFormat; }
            set { _requestFormat = value; }
        }

        private string _url;

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        private bool _responseSuccess;

        public bool ResponseSuccess
        {
            get { return _responseSuccess; }
            set { _responseSuccess = value; }
        }

        private StringContent _requestContent;

        public StringContent RequestContent
        {
            get { return _requestContent; }
            set { _requestContent = value; }
        }

        private string _responseContent;

        public string ResponseContent
        {
            get { return _responseContent; }
            set { _responseContent = value; }
        }

        private string _responseStatus;

        public string ResponseStatus
        {
            get { return _responseStatus; }
            set { _responseStatus = value; }
        }

        public void AsJson(string payload)
        {
            _requestContent = new StringContent(payload, Encoding.UTF8, "application/json");
        }

        public void AsXml(string payload)
        {
            _requestContent = new StringContent(payload, Encoding.UTF8, "application/xml");
        }

        public void RequestContentType(string contentType)
        {
            _requestContent.Headers.ContentType = new MediaTypeHeaderValue(contentType);
        }

        public void RequestPayload(string payload)
        {
            _requestContent = new StringContent(payload);
        }

        public void PostRequest()
        {
            var result = httpClient.PostAsync(Url, RequestContent).Result;
            ResponseSuccess = result.IsSuccessStatusCode;
            ResponseStatus = result.StatusCode.ToString();
            if (result.Content != null)
            {
                _responseContent = result.Content.ReadAsStringAsync().Result.ToString();
            }
        }

        public void GetRequest()
        {
            var result = httpClient.GetAsync(Url).Result;
            ResponseSuccess = result.IsSuccessStatusCode;
            ResponseStatus = result.StatusCode.ToString();
            if (result.Content != null)
            {
                _responseContent = result.Content.ReadAsStringAsync().Result.ToString();
            }
        }

        public bool AssertResponseContains(string searchString)
        {
            try
            {
                StringAssert.Contains(ResponseContent, searchString);
                return true;
            }
            catch (AssertFailedException)
            {
                return false;
            }
        }

        public void AddHeaderNameValue(string name, string value)
        {
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
            {
                httpClient.DefaultRequestHeaders.Add(name, value);
            }
        }

        public void AddContentType(string mediaType)
        {
            if (!string.IsNullOrEmpty(mediaType))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType));
            }
        }
    }
}