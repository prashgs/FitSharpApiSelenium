﻿using SeleniumFixtures.Pages;
using SeleniumFixtures.Validators;

namespace SeleniumFixtures
{
    public class WebFixture
    {
        public string Url { get; set; }
        public string SearchText { get; set; }

        public void OpenBrowser(string browser)
        {
            Browser.Init(browser);
        }

        public void OpenUrl(string url)
        {
            Browser.Goto(url);
        }

        public void SearchFor(string searchText)
        {
            GoogleHomePage googleHomePage = new GoogleHomePage();
            googleHomePage.Search(searchText);
        }

        public bool IsElementPresent(string searchBy, string locator)
        {
            var validator = new ElementValidator();
            return validator.ElementPresent(searchBy, locator);
        }

        public void CloseBrowser()
        {
            Browser.Close();
        }
    }
}