﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace FitnesseFixtures.Helpers
{
    public static class JsonHelper
    {
        public static T ReadJson<T>(string jsonFilePath)
        {
            using (StreamReader streamReader = new StreamReader(jsonFilePath))
            {
                var json = streamReader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(json);
            }
        }
    }
}