﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;

namespace SeleniumFixtures.Extension
{
    public static class WebElementExtensions
    {
        public static IWebElement FindElementAfterWait(this IWebDriver webDriver, By locator, int timeout = 20)
        {
            IWebElement element = null;
            try
            {
                new WebDriverWait(webDriver, TimeSpan.FromSeconds(timeout)).Until<bool>(drv =>
                {
                    try
                    {
                        element = drv.FindElement(locator);
                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                return element;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static IWebElement ClearAndSendKeysJs(this IWebElement webElement, string inputString)
        {
            try
            {
                for (int i = 0; i < 2; i++)
                {
                    if (!string.IsNullOrEmpty(inputString))
                    {
                        if (!inputString.Equals(webElement.GetAttribute("value"), StringComparison.OrdinalIgnoreCase))
                        {
                            IJavaScriptExecutor javaScriptExecutor = (IJavaScriptExecutor)Browser.Driver;
                            javaScriptExecutor.ExecuteScript("arguments[0].setAttribute('value','" + inputString + "')", webElement);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }

            return webElement;
        }

        public static IWebElement ClearAndSendKeys(this IWebElement webElement, string inputString)
        {
            try
            {
                for (int i = 0; i < 2; i++)
                {
                    if (!string.IsNullOrEmpty(inputString))
                    {
                        if (!inputString.Equals(webElement.GetAttribute("value"), StringComparison.OrdinalIgnoreCase))
                        {
                            webElement.Clear();
                            webElement.SendKeys(inputString);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }

            return webElement;
        }
    }
}