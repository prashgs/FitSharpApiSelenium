﻿using System;
using System.IO;
using System.Reflection;

namespace FitnesseFixtures.Helpers
{
    public static class FileHelper
    {
        public static string AssemblyPath
        {
            get
            {
                return Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            }
        }
    }
}