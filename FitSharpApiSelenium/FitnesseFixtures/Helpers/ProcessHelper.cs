﻿using System;
using System.Diagnostics;

namespace FitnesseFixtures.SeleniumFixture.Helpers
{
    public static class ProcessHelper
    {
        public static void Terminate(string processName)
        {
            try
            {
                var processes = Process.GetProcessesByName(processName);
                foreach (var process in processes)
                {
                    process.Kill();
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }
        }
    }
}